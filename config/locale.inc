<?php

return [
    'default' => 'pl-PL',
    'accepted' => [
        'pl-PL', 
        'en-US', 
        'en-GB'
    ]
];