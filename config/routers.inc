<?php

declare(strict_types=1);

use OptiFrame\Http\Router\DefaultRouter;

return [
    'default' => new DefaultRouter($request)
];