<?php

use OptiFrame\Library\Application;
use Symfony\Component\Dotenv\Dotenv;

define('APP_START', microtime(true));
define('APP_PATH', substr(__DIR__, 0, -7));

require_once APP_PATH . '/vendor/autoload.php';

(new Dotenv())->loadEnv(APP_PATH . '/.env');

$app = new Application('http');
$app->run('run');

$app->terminate();
